# Ludum Dare 30 Entry: ShiftedTimelines #
## Story ##
After creating your dimension shifter, you figured you should test it. This was your first mistake. When you hit the ironically red button you blacked out, and now you do not know where you are. the walls are all grey and in the distance you see nothing but nonsense, however your machine is still here with you. Maybe it can get you out? When you hit the button again you do not black out nor move to a distant land, but the terrain  of this strange place has. This world must be connected to another you think...
## Controls ##
Arrow keys: Move and jump
X: Use your dimension shifter
## Screenshots ##
![01.png](https://bitbucket.org/repo/GAaxKg/images/1766962182-01.png)