highscore = {}
local timer = 0
--createPlayers
require("/bin/console")
require("/bin/newCollisions")
require("/bin/gamestates/ex")
require("/bin/gamestates/ex2")
--require("/bin/gamestates/ex3")
require("/bin/gamestates/menu")
require("/bin/gamestates/victory")
require("/bin/player")
require("/bin/physics")
require("/bin/camera")

levels = {}
levels[1] = Ex
levels[2]= Ex2
levels[3] = Ex3
scoreHud = love.graphics.newImage("/resources/score.png")
player1img1 = love.graphics.newImage("/resources/player1.png")
portalImg = love.graphics.newImage("/resources/portal.png")
intro = love.audio.newSource("/resources/sound/intro LD30.wav", "stream")
loop = love.audio.newSource("/resources/sound/LoopLD30.wav", "stream")
transport = love.audio.newSource("/resources/sound/transport.wav", "static")
failedTransport = love.audio.newSource("/resources/sound/failedTransport.wav", "static")
jump = love.audio.newSource("/resources/sound/jump.wav", "static")
jump2 = love.audio.newSource("/resources/sound/jump2.wav", "static")
jump3 = love.audio.newSource("/resources/sound/jump3.wav", "static")
jumps = {
			jump,
			jump2,
			jump3
		}
		
fadeTimer = 0
		
step = love.audio.newSource("/resources/sound/step.wav", "static")

victoryPortal = {}
victoryPortal.img = love.graphics.newImage("/resources/sprites.png")
victoryPortal.fr1 = love.graphics.newQuad(0, 0, 64, 64, 324, 96)
victoryPortal.fr2 = love.graphics.newQuad(65, 0, 64, 64, 324, 96)
victoryPortal.fr3 = love.graphics.newQuad(130, 0, 64, 64, 324, 96)
victoryPortal.fr4 = love.graphics.newQuad(195, 0, 64, 64, 324, 96)
victoryPortal.fr5 = love.graphics.newQuad(260, 0, 64, 64, 324, 96)
victoryPortalImg = victoryPortal.fr1

coin = {}
coin.img = love.graphics.newImage("/resources/sprites.png")
coin.fr1 = love.graphics.newQuad(0, 64, 32, 32, 324, 96)
coin.fr2 = love.graphics.newQuad(33, 64, 32, 32, 324, 96)
coin.fr3 = love.graphics.newQuad(66, 64, 32, 32, 324, 96)
coinImg = coin.fr1

audiosrc = {}
audiosrc.bgm = intro 
muted = false

function love.load()
	gamestate = menu
	gamestate.load()
	camera:newLayer(.999, function()
		if inConsole then
			love.graphics.setColor(0,0,0)
			love.graphics.rectangle("fill",camera.x,camera.y,800,50)
			love.graphics.setColor(255,255,255)
			love.graphics.print(consoleStr, camera.x, camera.y, 0, 1, 1)
		else
			
		end
	end)
	
	dim = 1
	paused = false

	love.graphics.setBackgroundColor(40,40,40)
	medium = love.graphics.newFont(25)
	audiosrc.bgm:setVolume(.75)
	audiosrc.bgm:play()
end

function love.update(dt)
	gamestate.update(dt)

	if audiosrc.bgm:isStopped() and muted ~= true then
		audiosrc.bgm = loop
		audiosrc.bgm:setVolume(.5)
		audiosrc.bgm:setLooping(true)
		audiosrc.bgm:play()
	end
	if inConsole or paused then return end
	timer = timer + dt
	if timer > 1 then 
		timer = 0 
	elseif timer > 0.8 then
		victoryPortalImg = victoryPortal.fr2
	elseif timer > 0.6 then
		victoryPortalImg = victoryPortal.fr3
	elseif timer > 0.4 then
		victoryPortalImg = victoryPortal.fr4
	elseif timer > 0.2 then 
		victoryPortalImg = victoryPortal.fr5
	else
		victoryPortalImg = victoryPortal.fr1
	end
	if timer > 2/3 then 
		coinImg = coin.fr3
	elseif timer > 1/3 then
		coinImg = coin.fr2
	else
		coinImg = coin.fr1
	end
end

function love.draw()
	camera:draw()
end

function drawTile(x, y, w, h, r, g, b, a, i)
	love.graphics.setColor(r,g,b,a)
	love.graphics.rectangle("fill",x,y,w,h)
	love.graphics.setColor(255,255,255,255)
end

function love.keypressed(key, unicode)
	if inConsole == false then
		gamestate.keypressed(key)
		if gamestate~= menu then if key == escape then gamestate = menu end end
		if key == "`" then inConsole = true end
		if key == "m" then
			if muted == false then
				muted = true
				for k, v in pairs(audiosrc) do
					v:stop()
				end
			elseif muted == true then
				audiosrc.bgm = intro
				audiosrc.bgm:play()
				muted = false
			else
				print("error")
			end
		end
	else
		console.keypressed(key)
	end
end

function love.textinput(t)
	--print(t)
	if inConsole then
		console.textinput(t)
	end
end

function love.keyreleased(key, unicode)
	gamestate.keyreleased(key)
end

function love.mousepressed(x, y, button)
	print(x .. ", " .. y)
	gamestate.mousepressed(x, y, button)
end

function love.mousereleased(x, y, button)
end

function playSound(src)
	if muted == false then
		src:play()
	end
end


