local timer = 0
moveable = true

player = {}

player.name = "player"
player.velocityY = 0
player.velocityX = 0
player.velocityMaxX = 200
player.velocityMinX = -200

player.jumpForce = 200
score = 0
player.gravity = 0

player.collisions = {
	top = false,
	bottom = false,
	left = false,
	right = false,
	oTop = false,
	oBottom = false,
	oLeft = false,
	oRight = false
}

function player.load()
	player.x = gamestate.spawnPointX
	player.y = gamestate.spawnPointY
	player.width = 64
end

function player.update(dt)
	if continue == true then moveable = false end
	newCollisions(player, dt)
	checkOtherTiles(player, dt)
	--Camera Follow
	camera.x = player.x +32-400
	camera.y = player.y+96-600
	if moveable == true then 
		--Physics
		doPhysics(player, dt)
			--Friction
			if player.collisions.bottom == true then
				if love.keyboard.isDown("left") == false and love.keyboard.isDown("right") == false then
					if player.velocityX < 0 then
						player.velocityX = player.velocityX + 50*dt
					elseif player.velocityX > 0 then
						player.velocityX = player.velocityX - 50*dt
					end
				end
			end
			if player.velocityX < 1 and player.velocityX > 0-1 then player.velocityX = 0 end
		--Movement
		if love.keyboard.isDown("left") then
			if player.velocityMinX < player.velocityX then
				player.velocityX = player.velocityX - 500*dt
			end
		elseif love.keyboard.isDown("right") then
			if player.velocityMaxX > player.velocityX then
				player.velocityX = player.velocityX + 500*dt
			end
		end
		--Jumping
		if player.collisions.bottom == true then
			if love.keyboard.isDown("up") then
				print("jumpAttempt")
				player.velocityY = 0-player.jumpForce
				for k, v in pairs(jumps) do
					love.audio.stop(v)
				end
				local rand = love.math.random(0,3)
				if rand > 2 then 
					playSound(jump)
				elseif rand > 1 then
					playSound(jump2)
				else
					playSound(jump3)
				end
				
			end
		end
	else
		print (timer)
		if timer == nil then timer = 0 end
		timer = timer + dt
		if timer > 1 then 
			if timer > 1.005 then timer = 0 end
			if continue == true then
				if gamestate == Ex then
					camera:clear()
					Ex2.load()
				else
					victory.load()
				end
			end
			moveable = true 
		end
	end
	if player.collisions.bottom ~= true then
		step:stop()
	end
	--Death/Suffocation
	if player.collisions.top == true and player.collisions.bottom == true then
		player.x = player.x + 32
		if player.collisions.top == true and player.collisions.bottom == true then
			player.load()
		end
	end
	if player.y > 800 then gamestate.load() end
	if player.collisions.left == true and player.collisions.right == true then
		player.y = player.y - 4
	end
	if timer > 1 then timer = 0 end
end

function player.draw()
	if continue == false then
		if player.velocityX < 0 then 
			love.graphics.draw(player1img1, player.x + 64, player.y, 0, -1, 1)
		else
			love.graphics.draw(player1img1, player.x, player.y, 0, 1, 1)
		end
	end
	if moveable == false then
		if continue == false then
			love.graphics.setColor(255,255,255,timer*255)
			if player.velocityX < 0 then
				love.graphics.draw(portalImg, player.x - 4, player.y, 0, 1, 1)
			else
				love.graphics.draw(portalImg, player.x, player.y, 0, 1, 1)
			end
		else
			if player.velocityX < 0 then 
				love.graphics.draw(player1img1, player.x +32, player.y, timer*360, -1 + timer, 1 - timer, 32, 32)
			else
				love.graphics.draw(player1img1, player.x +32 , player.y, timer*360, 1-timer, 1-timer, 32, 32)
			end
		end
	end
	
	love.graphics.setColor(255,255,255)
end

function player.keypressed(key)
	if key == "x" then
		switchDim()
	end
	if player.collisions.bottom == true then
		if key == "left" or key == "right" then
			step:setLooping(true)
			playSound(step)
		end
	end
	if key == "a" then
		print(player.x)
	end
end

function player.keyreleased(key)
	if key == "left" or key == "right" then
		step:setLooping(true)
		step:stop()
	end
end

function switchDim()
	if player.collisions.oLeft == true and player.collisions.oRight == true then
		playSound(failedTransport)
	elseif player.collisions.oTop == true and player.collisions.oBottom == true then
		playSound(failedTransport)
	else
		timer = 0
		if dim == 1 then
			dim =2
		else
			dim = 1
		end
		moveable = false
		fade = true
		playSound(transport)
	end
end

function collect(obj, v)
	--print(v.numb .. "," .. #dim1.tiles .. "," .. #dim2.tiles )
	if dim == 1 then
		table.remove(dim1.tiles, v.numb)
	else
		table.remove(dim2.tiles, v.numb)
	end
	
	if obj == "coin" then
		local givenScore = false
		if score < #coins and givenScore == false then	
			score = score + 1
			givenScore = true
		end
		
		--print(v.collected)	
	end
	
end