victory = {}
print ("victory loaded")
map = victory
victory.spawnPointX = 20
victory.spawnPointY = 600-256
victoryBg = {}
victoryBg.foreground = love.graphics.newImage("/resources/groundAndText.png")
victoryBg.backGround = love.graphics.newImage("/resources/cloudBackdrop.png")
victoryBg.midGround = love.graphics.newImage("/resources/house.png")
local function loadTiles()
	dim1 = {}
	dim1.tiles = {
		{
			x = 0, y = 600 - 120, w = 250, h = 32, r = 50, g = 50, b = 50, a = 255, numb = 1
		},
		{
			x = 0, y = 600 - 100, w = 350, h = 32, r = 50, g = 50, b = 50, a = 255, numb = 1
		},
		{
			x = 0, y = 600 - 80, w = 425, h = 32, r = 50, g = 50, b = 50, a = 255, numb = 1
		},
		{
			x = -400, y = 600 - 75, w = 3200, h = 32, r = 50, g = 50, b = 50, a = 255, numb = 1
		},
		{
			x = -10, y = -400, w = 19, h = 1200, r = 50, g = 50, b = 50, a = 0, numb = 1
		},
		{
			x = 725, y = -400, w = 32, h = 1200, r = 50, g = 50, b = 50, a = 0, numb = 1
		},
	}

	dim2 = {}
	dim2.tiles = {
		{
			x = -400, y = 600 - 64, w = 3200, h = 32, r = 50, g = 50, b = 50, a = 255, numb = 1
		},
		{
			x = -10, y = -400, w = 19, h = 1200, r = 50, g = 50, b = 50, a = 0, numb = 1
		},
		{
			x = 725, y = -400, w = 32, h = 1200, r = 50, g = 50, b = 50, a = 0, numb = 1
		},
	}
end
function victory.load()
	continue = false
	gamestate = victory
	fadeAlpha = 0
	camera:clear()
	score = 0
	loadTiles()
	tiles = dim1.tiles
	player.load()
	
	camera:newLayer(1, function ()
		love.graphics.draw(victoryBg.midGround, -400, -300)
		player.draw()
		
	end)
	
	camera:newLayer(.25, function ()
		love.graphics.draw(victoryBg.backGround, -400, -300)
	end)
	camera:newLayer(1.1, function()
		love.graphics.draw(victoryBg.foreground, -400, -300)
	end)
	
	fadeAlpha = 0
	
	coins = {}
	for k, v in pairs(dim1.tiles) do
		if v.string == "coin" then
			table.insert(coins, v)
		end
	end
	for k, v in pairs(dim2.tiles) do
		if v.string == "coin" then
			table.insert(coins, v)
		end
	end
end

function victory.update(dt)
	if dim == 1 then
		tiles = dim1.tiles
	else
		tiles = dim2.tiles
	end
	if fade == true then
		fadeTimer = fadeTimer+dt
		
		if fadeTimer < .5 then fadeAlpha = fadeTimer*255*2 else fadeAlpha = (1 - fadeTimer)*255*2 end
		if fadeTimer > 1 then fadeTimer = 0 end
		if fadeTimer == 0 then fade = false end
	end
	player.update(dt)
end

function victory.draw()
	
end

function victory.keypressed(key)
	player.keypressed(key)
end

function victory.keyreleased(key)
	player.keyreleased(key)
end

function victory.mousepressed(x, y, button)

end

function victory.mousereleased(x, y, button)

end

function victory.tvictorytinput(t)

end