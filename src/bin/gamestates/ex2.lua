Ex2 = {}
print ("Ex2 loaded")
map = Ex2
Ex2.spawnPointX = 20
Ex2.spawnPointY = 600-128
Ex2bg = love.graphics.newImage("/resources/Ex2dg.png")
Ex2dg = love.graphics.newImage("/resources/Ex2bg.png")
local function loadTiles()
	dim1 = {}
	dim1.tiles = {
		{
			x = -400, y = 600 - 32, w = 3200, h = 32, r = 50, g = 50, b = 50, a = 255, numb = 1
		},
		{
			x = -400, y = 0, w = 20, h = 600, r = 50, g = 50, b = 50, a = 255, numb = 2
		},
		{
			x = 3180, y = 0, w = 20, h = 600, r = 50, g = 50, b = 50, a = 255, numb = 3
		},
		{
			x = 400, y = 400, w = 128, h = 32, r = 50, g = 50, b = 50, a = 255, numb = 4
		},
		{
			x = 200, y = 500, w = 128, h = 32, r = 50, g = 50, b = 50, a = 255, numb = 5
		},
		{
			x = 256, y = 200, w = 128, h = 32, r = 50, g = 50, b = 50, a = 255, numb = 6
		},
		{
			x = 0, y = 500-64, w = 32, h = 32, r = 255, g = 255, b = 0, a = 0, special = true, string = "coin", numb = 7, trigger = true
		},
		{
			x = 320, y = 136+64-8, w = 8, h = 8, r = 50, g = 50, b = 50, a = 0, trigger = true, special = true, image = victoryPortalImg, string = "victory"
		},
		{
			x = 256, y = 0, w = 128, h = 32, r = 50, g = 50, b = 50, a = 255, numb = 9
		},
		
		{
			x = 256, y = -100, w = 32, h = 32, r = 255, g = 255, b = 0, a = 0, special = true, string = "coin", numb = dim1Tiles, trigger = true
		},
	}
	
	dim2 = {}
	dim2.tiles = {
		{
			x = -400, y = 600 - 32, w = 3200, h = 32, r = 50, g = 50, b = 50, a = 255, numb = 1
		},
		{
			x = -400, y = 0, w = 20, h = 600, r = 50, g = 50, b = 50, a = 255, numb = 2
		},
		{
			x = 3200-400, y = 0, w = 20, h = 600, r = 50, g = 50, b = 50, a = 255, numb = 3
		},
		{
			x = 400, y = 400, w = 128, h = 32, r = 50, g = 50, b = 50, a = 255, numb = 4
		},
		{
			x = 600, y = 300, w = 128, h = 32, r = 50, g = 50, b = 50, a = 255
		},
		{
			x = 780, y = 0, w = 20, h = 600, r = 50, g = 50, b = 50, a = 255
		},
		{
			x = 1000, y = 500-64, w = 32, h = 32, r = 255, g = 255, b = 0, a = 0, special = true, string = "coin", numb = 7, trigger = true
		},
		{
			x = 600-40, y = 600-64-32, w = 64, h = 8, r = 50, g = 50, b = 50, a = 0, trigger = true, special = true, image = victoryPortalImg, string = "victory"
		},
		{
			x = 56, y = 150, w = 128, h = 32, r = 50, g = 50, b = 50, a = 255, numb = 8
		},
	}
end
function Ex2.load()
	continue = false
	gamestate = Ex2
	fadeAlpha = 0
	camera:clear()
	score = 0
	loadTiles()
	tiles = dim1.tiles
	player.load()
	
	camera:newLayer(1, function ()
		for k, i in pairs(tiles) do
			drawTile(i.x, i.y, i.w, i.h, i.r, i.g, i.b, i.a, i.image)
		end
		love.graphics.draw(victoryPortal.img, victoryPortalImg, 600-40, 600-64-32, 0, 1, 1)
		for k, v in pairs(tiles) do
			if v.string == "coin" then
				love.graphics.draw(coin.img, coinImg, v.x, v.y, 0, 1, 1)
			end
		end
		player.draw()
		love.graphics.draw(scoreHud, camera.x+400-156, camera.y+5, 0, 1, 1, 0, 0)
			love.graphics.setFont(medium)
			love.graphics.setColor(0,0,0)
			love.graphics.print(score .. "/" .. #coins, camera.x + 390, camera.y + 20, 0, 1, 1, 0, 0)
			love.graphics.setColor(255,255,255)
	end)
	
	camera:newLayer(.5, function ()
		if dim == 1 then love.graphics.draw(Ex2bg, -400, -300) else love.graphics.draw(Ex2dg, -400, - 300) end
	end)
	camera:newLayer(.9, function()
		if fade == true then
			love.graphics.setColor(0,0,0,fadeAlpha)
			love.graphics.rectangle("fill", camera.x , camera.y, 800,600)
			love.graphics.setColor(255,255,255,255)
		end
	end)
	
	fadeAlpha = 0
	
	coins = {}
	for k, v in pairs(dim1.tiles) do
		if v.string == "coin" then
			table.insert(coins, v)
		end
	end
	for k, v in pairs(dim2.tiles) do
		if v.string == "coin" then
			table.insert(coins, v)
		end
	end
end

function Ex2.update(dt)
	dim1Tiles = #dim1.tiles
	if dim == 1 then
		tiles = dim1.tiles
	else
		tiles = dim2.tiles
	end
	if fade == true then
		fadeTimer = fadeTimer+dt
		
		if fadeTimer < .5 then fadeAlpha = fadeTimer*255*2 else fadeAlpha = (1 - fadeTimer)*255*2 end
		if fadeTimer > 1 then fadeTimer = 0 end
		if fadeTimer == 0 then fade = false end
	end
	player.update(dt)
end

function Ex2.draw()
	
end

function Ex2.keypressed(key)
	player.keypressed(key)
end

function Ex2.keyreleased(key)
	player.keyreleased(key)
end

function Ex2.mousepressed(x, y, button)

end

function Ex2.mousereleased(x, y, button)

end

function Ex2.textinput(t)

end