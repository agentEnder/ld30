maxGravity =  128
gravityRate = 19.6
gravity = 0

function doPhysics(obj, dt)
	--print("doPhysics(" .. obj.name .. ")")
	
	--Vertical Physics
	if obj.velocityY == nil then obj.velocityY = 0 end
	if obj.collisions.bottom == false then
		obj.velocityY = obj.velocityY + obj.gravity*dt
		if obj.gravity < maxGravity then
			obj.gravity = obj.gravity + gravityRate
		end
	else
		obj.gravity = 0
		if obj.velocityY > 0 then
			obj.velocityY = 0
		end
	end
	if obj.collisions.top == true then
		obj.velocityY = -1
	end
	obj.y = obj.y + obj.velocityY*dt
	
	--Horizontal Physics
	if obj.collisions.left == true then
		if obj.velocityX < 0 then obj.velocityX = 0 end
	end
	if obj.collisions.right == true then
		if obj.velocityX > 0 then obj.velocityX = 0 end
	end
	
	obj.x = obj.x+obj.velocityX*dt
end
